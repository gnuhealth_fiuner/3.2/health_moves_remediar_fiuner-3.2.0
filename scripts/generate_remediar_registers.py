import csv

xml_header = '<?xml version="1.0" encoding="utf-8"?>\n'\
                +'<tryton>\n'\
                +'<data noupdate="1">\n\n'\
                +'<!-- Add basic category\n\n -->'\
                +'<record model="product.category" id="prod_medicament_REMEDIAR">\n'\
                +'        <field name="name">REMEDIAR</field>\n'\
                +'        <field name="parent" ref="prod_medicament"/>\n'\
                +'</record>\n\n'
xml_templates = []  #product.template list
xml_products = []   #product.product list
xml_cat_templ = []  #product.categories list
xml_medicines = []  #gnuhealth.medicines list

# Start to fill the template, product, category and medicine list
# with the data on the csv
with open('remediar_list.csv', 'rb') as csvfile:
    source = csv.reader(csvfile, delimiter=':')    
    xml_templates.append('\n<!-- PRODUCT TEMPLATE DEFINITION -->\n\n')
    xml_products.append('\n<!-- PRODUCT DEFINITION -->\n\n')
    xml_cat_templ.append('\n<!-- CATEGORIES -->\n\n')
    xml_medicines.append(
            '<?xml version="1.0" encoding="utf-8"?>\n'\
            +'<tryton>\n'\
            +'<data skiptest="1" noupdate="0">\n\n'\
            +'<!-- Add list of products -->\n')
    for product_name,id_templ, id_prod, id_cat_templ, id_medicine in source:
        xml_templates.append(
            '<record model="product.template" id="'+id_templ+'"> \n'\
            +'    <field name="name">'+product_name+'</field> \n'\
            +'    <field name="default_uom" model="product.uom" ref="product.uom_unit"/>\n'\
            +'    <field name="list_price" eval="0.0"/>\n'\
            +'    <field name="cost_price" eval="0.0"/>\n'\
            +'</record>\n'
        )
        xml_products.append(
            '<record model="product.product" id="'+id_prod+'">\n'\
            +'    <field name="template" model="product.template" ref='+id_templ+'"/>\n'\
            +'    <field name="is_medicament">1</field>\n'\
            +'</record>\n'
        )
        xml_cat_templ.append(
            '<record model="product.template-product.category" id="'+id_cat_templ+'">\n'\
            +'<field name="template" ref="'+id_templ+'"/>\n'\
            +'<field name="category" ref="prod_medicament_REMEDIAR"/>\n'\
            +'</record>\n'            
        )   
        xml_medicines.append(
            '<record model="gnuhealth.medicament" id="'+id_medicine+'">\n'\
            +'    <field name="name" model="product.product" ref="'+id_prod+'"/>\n'\
            +'    <field name="category" model="gnuhealth.medicament.category" ref="REMEDIAR"/>\n'\
            +'</record>\n'
            )        
xml_footer ='\n</data>\n</tryton>'

#Start to fill the templates, products, categories data to import
with open('REMEDIAR_products.xml','w') as output_xml:
    output_xml.write(xml_header)    
    for line in xml_templates:
        output_xml.write(line)
    for line in xml_products:
        output_xml.write(line)
    for line in xml_cat_templ:
        output_xml.write(line)
    output_xml.write(xml_footer)

#Start to fill the medicines data to import
with open('REMEDIAR_list_of_medicines.xml','w')as output_xml:
    for line in xml_medicines:
        output_xml.write(line)
    output_xml.write(xml_footer)
